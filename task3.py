from collections import deque


def create_default_graph():
    G = {"1": ["4"],
         "4": ["2", "3"],
         "3": ["2"],
         "2": []}
    return G


def dfs(graph):
    to_visit = deque(next(iter(graph)))
    visited = set()
    stack = []
    while to_visit:
        node = to_visit.pop()
        visited.add(node)
        if node not in stack:
            stack.append(node)
        for neighbour in graph[node]:
            if neighbour in visited:
                continue
            to_visit.append(neighbour)
    return stack


def topological_sort(graph):
    stack_node = dfs(graph)
    number = 1
    for stack_element in stack_node:
        graph[number] = graph.pop(stack_element)
        for node in graph:
            new_nodes = []
            for node_element in graph[node]:
                if node_element == stack_element:
                    new_nodes.append(number)
                else:
                    new_nodes.append(node_element)
            graph[node] = new_nodes
        number += 1
    return graph


def run():
    graph = create_default_graph()
    result = topological_sort(graph)
    return result


if __name__ == "__main__":
    run()
