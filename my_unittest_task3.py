import unittest
import task3
class TestMethods(unittest.TestCase):
    def test_test_default(self):
        self.assertEqual(task3.run(),  {1: [2],
         2: [4, 3],
         3: [4],
         4: []})

if __name__ == '__main__':
    unittest.main()